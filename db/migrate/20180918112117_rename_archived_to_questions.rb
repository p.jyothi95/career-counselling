class RenameArchivedToQuestions < ActiveRecord::Migration[5.1]
  def change
    rename_column :questions, :archieved, :archived
  end
end
