class AddColumnWorkshopToEvents < ActiveRecord::Migration[5.1]
  def change
    add_column :events, :workshop_type, :string
  end
end
