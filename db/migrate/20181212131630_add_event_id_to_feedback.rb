class AddEventIdToFeedback < ActiveRecord::Migration[5.1]
  def change
    add_column :student_feedbacks, :event_id, :integer
  end
end
