class AddGuestUserIdToAssignments < ActiveRecord::Migration[5.1]
  def change
    add_column :assignments, :guest_user_id, :integer
  end
end
