class AddSlugToTests < ActiveRecord::Migration[5.1]
  def self.up
    add_column :tests, :slug, :string

    Test.where(slug: nil).each do |test|
      test.generate_slug
      test.save
    end
  end

  def self.down
    remove_column :tests, :slug
  end
end
