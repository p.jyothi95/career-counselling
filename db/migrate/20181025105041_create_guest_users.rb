class CreateGuestUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :guest_users do |t|
      t.string :name
      t.string :phone
      t.string :email
      t.string :address
      t.timestamps
    end
  end
end
