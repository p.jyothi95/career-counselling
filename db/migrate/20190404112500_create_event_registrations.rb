class CreateEventRegistrations < ActiveRecord::Migration[5.1]
  def change
    create_table :event_registrations do |t|
      t.string :name
      t.string :mobile
      t.string :email
      t.string :company
      t.text :extra

      t.timestamps
    end
  end
end
