class CreateGfsRegistrations < ActiveRecord::Migration[5.1]
  def change
    create_table :gfs_registrations do |t|
      t.string :name
      t.date :date_of_birth
      t.string :state
      t.string :city
      t.string :email
      t.string :mobile
      t.string :phone
      t.string :martial_status
      t.string :dependents
      t.string :citizen
      t.string :pan
      t.string :expirence
      t.string :income
      t.string :amount
      t.boolean :existing_loan
      t.string :attachment1
      t.string :attachment2
      t.string :attachment3
      t.string :financial_service_intrested
      t.string :funding_attachment
      t.text :references
      

      t.timestamps
    end
  end
end
