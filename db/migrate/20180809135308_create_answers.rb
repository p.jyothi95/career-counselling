class CreateAnswers < ActiveRecord::Migration[5.1]
  def change
    create_table :answers do |t|
      t.integer :question_id
      t.string :description
      t.boolean :is_correct, default: false
      t.integer :position
      t.boolean :archieved, default: false
      t.timestamps
    end
  end
end
