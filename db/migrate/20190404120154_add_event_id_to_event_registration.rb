class AddEventIdToEventRegistration < ActiveRecord::Migration[5.1]
  def change
    add_column :event_registrations, :event_id, :integer
  end
end
