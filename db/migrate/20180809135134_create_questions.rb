 class CreateQuestions < ActiveRecord::Migration[5.1]
  def change
    create_table :questions do |t|
      t.text :text
      t.string :category
      t.integer :q_type
      t.boolean :archieved, default: false
      t.timestamps
    end
  end
end
