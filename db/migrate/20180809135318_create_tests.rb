class CreateTests < ActiveRecord::Migration[5.1]
  def change
    create_table :tests do |t|
      t.string :name
      t.text :guidelines
      t.integer :duration
      t.boolean :archieved, default: false
      t.timestamps
    end
  end
end
