class AddColumnsToEvents < ActiveRecord::Migration[5.1]
  def change
    add_column :events, :is_recurring, :boolean, default: false
    add_column :events, :frequency, :string
    add_column :events, :recurring_end_date, :date
  end
end
