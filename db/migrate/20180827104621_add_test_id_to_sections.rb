class AddTestIdToSections < ActiveRecord::Migration[5.1]
  def change
    add_column :sections, :test_id, :integer
  end
end
