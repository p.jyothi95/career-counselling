class CreateSectionQuestions < ActiveRecord::Migration[5.1]
  def change
    create_table :section_questions do |t|
      t.integer :section_id
      t.integer :question_id
      t.timestamps
    end
  end
end
