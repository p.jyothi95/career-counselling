class AddColumnEventScheduleToEvents < ActiveRecord::Migration[5.1]
  def change
    add_column :events, :next_event_scheduled, :boolean, default: false
  end
end
