class CreateSections < ActiveRecord::Migration[5.1]
  def change
    create_table :sections do |t|
      t.string :name
      t.string :category
      t.text :guidelines
      t.boolean :archieved, default: false
      t.timestamps
    end
  end
end
