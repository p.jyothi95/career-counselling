class CreateMessages < ActiveRecord::Migration[5.1]
  def change
    create_table :messages do |t|
      t.integer :discussion_id
      t.integer :sender_id
      t.string :text
      t.boolean :is_read
      t.timestamps
    end
  end
end
