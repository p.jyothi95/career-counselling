class RenameArchivedToAnswers < ActiveRecord::Migration[5.1]
  def change
    rename_column :answers, :archieved, :archived
  end
end
