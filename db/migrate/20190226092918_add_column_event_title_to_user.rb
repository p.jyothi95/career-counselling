class AddColumnEventTitleToUser < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :event_title, :string
  end
end
