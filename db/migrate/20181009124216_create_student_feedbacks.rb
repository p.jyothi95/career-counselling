class CreateStudentFeedbacks < ActiveRecord::Migration[5.1]
  def change
    create_table :student_feedbacks do |t|
      t.string :name
      t.date :date
      t.string :email
      t.string :phone
      t.string :program_title
      t.string :organisation
      t.text :top_3_things
      t.text :expirence
      t.text :implementation_on_day_to_day
      t.string :rsvp
      t.timestamps
    end
  end
end
