class RenameArchivedToTests < ActiveRecord::Migration[5.1]
  def change
    rename_column :tests, :archieved, :archived
  end
end
