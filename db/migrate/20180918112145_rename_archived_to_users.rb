class RenameArchivedToUsers < ActiveRecord::Migration[5.1]
  def change
    rename_column :users, :archieved, :archived
  end
end
