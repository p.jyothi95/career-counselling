class RenameArchivedToAssignments < ActiveRecord::Migration[5.1]
  def change
    rename_column :assignments, :archieved, :archived
  end
end
