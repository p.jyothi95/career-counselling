class AddRenameGfsRegistrations < ActiveRecord::Migration[5.1]
  def change
    rename_column :gfs_registrations, :attachment1, :sanction_letter_attachment
    rename_column :gfs_registrations, :attachment2, :outstanding_loan
    rename_column :gfs_registrations, :attachment3, :emi
    rename_column :gfs_registrations, :existing_loan, :terms_conditions
    add_column :gfs_registrations, :revenue, :string
    add_column :gfs_registrations, :money_need, :string
    add_column :gfs_registrations, :itr_years, :string
    add_column :gfs_registrations, :profitable, :text
    add_column :gfs_registrations, :balance_tenure, :string

  end
end
