class AddSuggestionsToStudentFeedback < ActiveRecord::Migration[5.1]
  def change
    add_column :student_feedbacks, :suggestions, :text
  end
end
