class RenameArchivedToSections < ActiveRecord::Migration[5.1]
  def change
    rename_column :sections, :archieved, :archived
  end
end
