class CreateAssignments < ActiveRecord::Migration[5.1]
  def change
    create_table :assignments do |t|
      t.integer :test_id
      t.integer :user_id
      t.date :due_date
      t.string :status
      t.datetime :start_time
      t.datetime :end_time
      t.text :feedback
      t.text :notes
      t.timestamps
    end
  end
end
