class ArchievedColumnToAssignments < ActiveRecord::Migration[5.1]
  def change
    add_column :assignments, :archieved, :boolean, default: :false
  end
end
