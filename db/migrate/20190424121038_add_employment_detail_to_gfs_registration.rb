class AddEmploymentDetailToGfsRegistration < ActiveRecord::Migration[5.1]
  def change
    add_column :gfs_registrations, :employment_detail, :string
  end
end
