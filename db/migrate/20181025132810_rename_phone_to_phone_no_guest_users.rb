class RenamePhoneToPhoneNoGuestUsers < ActiveRecord::Migration[5.1]
  def change
    rename_column :guest_users, :phone, :phone_no
  end
end
