class AddColumnSpeakerImageToEvents < ActiveRecord::Migration[5.1]
  def change
    add_column :events, :speaker_image, :string
  end
end
