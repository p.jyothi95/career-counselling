class AddArchievedToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :archieved, :boolean, default: :false
  end
end
