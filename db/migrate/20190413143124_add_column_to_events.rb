class AddColumnToEvents < ActiveRecord::Migration[5.1]
  def change
    add_column :events, :map_embed_code, :text
    add_column :events, :reg_image, :string
    add_column :events, :speaker, :text
  end
end
