class CreateEvents < ActiveRecord::Migration[5.1]
  def change
    create_table :events do |t|
      t.string :name
      t.text :description
      t.date :event_date
      t.date :reg_end_date

      t.timestamps
    end
  end
end
