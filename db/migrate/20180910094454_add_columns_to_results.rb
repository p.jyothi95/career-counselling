class AddColumnsToResults < ActiveRecord::Migration[5.1]
  def change
    add_column :results, :assignment_id, :integer
    add_column :results, :answers, :text
  end
end
