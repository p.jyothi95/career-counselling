# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20190424121038) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "answers", force: :cascade do |t|
    t.integer "question_id"
    t.string "description"
    t.boolean "is_correct", default: false
    t.integer "position"
    t.boolean "archived", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "assignments", force: :cascade do |t|
    t.integer "test_id"
    t.integer "user_id"
    t.date "due_date"
    t.string "status"
    t.datetime "start_time"
    t.datetime "end_time"
    t.text "feedback"
    t.text "notes"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "archived", default: false
    t.integer "guest_user_id"
  end

  create_table "event_registrations", force: :cascade do |t|
    t.string "name"
    t.string "mobile"
    t.string "email"
    t.string "company"
    t.text "extra"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "event_id"
  end

  create_table "events", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.datetime "event_date"
    t.date "reg_end_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "category"
    t.text "venue"
    t.string "image"
    t.integer "duration"
    t.boolean "is_recurring", default: false
    t.string "frequency"
    t.date "recurring_end_date"
    t.boolean "next_event_scheduled", default: false
    t.text "map_embed_code"
    t.string "reg_image"
    t.text "speaker"
    t.string "workshop_type"
    t.string "speaker_image"
  end

  create_table "gfs_registrations", force: :cascade do |t|
    t.string "name"
    t.date "date_of_birth"
    t.string "state"
    t.string "city"
    t.string "email"
    t.string "mobile"
    t.string "phone"
    t.string "martial_status"
    t.string "dependents"
    t.string "citizen"
    t.string "pan"
    t.string "expirence"
    t.string "income"
    t.string "amount"
    t.boolean "terms_conditions"
    t.string "sanction_letter_attachment"
    t.string "outstanding_loan"
    t.string "emi"
    t.string "financial_service_intrested"
    t.string "funding_attachment"
    t.text "references"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "revenue"
    t.string "money_need"
    t.string "itr_years"
    t.text "profitable"
    t.string "balance_tenure"
    t.string "employment_detail"
  end

  create_table "guest_users", force: :cascade do |t|
    t.string "name"
    t.string "phone_no"
    t.string "email"
    t.string "address"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "messages", force: :cascade do |t|
    t.integer "discussion_id"
    t.integer "sender_id"
    t.string "text"
    t.boolean "is_read", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "questions", force: :cascade do |t|
    t.text "text"
    t.string "category"
    t.integer "q_type"
    t.boolean "archived", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "is_deleted", default: false
    t.boolean "is_selected", default: false
  end

  create_table "results", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "assignment_id"
    t.text "answers"
  end

  create_table "section_questions", force: :cascade do |t|
    t.integer "section_id"
    t.integer "question_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "sections", force: :cascade do |t|
    t.string "name"
    t.string "category"
    t.text "guidelines"
    t.boolean "archived", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "test_id"
  end

  create_table "student_feedbacks", force: :cascade do |t|
    t.string "name"
    t.date "date"
    t.string "email"
    t.string "phone"
    t.string "program_title"
    t.string "organisation"
    t.text "top_3_things"
    t.text "expirence"
    t.text "implementation_on_day_to_day"
    t.string "rsvp"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "event_id"
    t.text "suggestions"
  end

  create_table "tests", force: :cascade do |t|
    t.string "name"
    t.text "guidelines"
    t.integer "duration"
    t.boolean "archived", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "slug"
  end

  create_table "user_events", force: :cascade do |t|
    t.integer "user_id"
    t.integer "event_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "role"
    t.string "name"
    t.integer "string"
    t.string "address"
    t.boolean "archived", default: false
    t.string "phone_no"
    t.string "school_college_institution"
    t.string "event_title"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

end
