// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
// require turbolinks
//= require ./jquery-3.2.1.min
//= require ./popper
//= require ./bootstrap.min
//= require ./plugins/greensock/TweenMax.min
//= require ./plugins/greensock/TimelineMax.min
//= require ./plugins/scrollmagic/ScrollMagic.min
//= require ./plugins/greensock/animation.gsap.min
//= require ./plugins/greensock/ScrollToPlugin.min
//= require ./plugins/OwlCarousel2-2.2.1/owl.carousel
//= require ./plugins/easing/easing
//= require ./plugins/parallax-js-master/parallax.min
//= require ./js/custom

