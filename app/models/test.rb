class Test < ApplicationRecord
  DURATIONS = {15 => '15 minutes', 20 => '20 minutes', 30 => '30 minutes', 40 => '40 minutes', 60 => '60 minutes', 90 => '90 minutes', 120 => '120 minutes'}
  has_many :assignments
  has_many :sections
  has_many :section_questions, through: :sections

  validates :name, :duration, :guidelines, presence: true
  validates :slug, presence: true, uniqueness: true

  before_validation :generate_slug, on: :create

  def generate_slug
    self.slug = self.name.parameterize
    count = 0
    while Test.find_by(slug: self.slug).present?
      count++
      self.slug = self.name.parameterize + "-#{count}"
    end
  end
end
