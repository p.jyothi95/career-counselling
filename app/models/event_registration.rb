class EventRegistration < ApplicationRecord
  serialize :extra, JSON
  belongs_to :event
end
