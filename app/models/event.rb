class Event < ApplicationRecord
  CATEGORIES = ['anthropreneur', 'job_seeker', 'general']
  FREQUENCIES = ['daily', 'weekly', 'monthly']
  DURATIONS = { 30 => '30 minutes', 40 => '40 minutes', 60 => '1 hour', 90 => '1.5 hours', 120 => '2 hours', 150 => '2.5 hours', 180 => '3 hours', 210 => '3.5 hours', 240 => '4 hours' }

  has_many :student_feedbacks
  has_many :user_events
  has_many :event_registrations

  scope :active, -> { where('event_date >= ?', Time.current.beginning_of_day) }
  scope :expired, -> { where('event_date < ?', Time.current.beginning_of_day) }

  validates :name, :event_date, :reg_end_date, :category,  presence: true

  def image_url
    image || "event_1.jpg"
  end

  def frequency_duration
    case frequency
    when 'daily'
      1.day
    when 'weekly'
      1.week
    when 'monthly'
      1.month
    end
  end
end
