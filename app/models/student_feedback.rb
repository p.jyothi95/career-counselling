class StudentFeedback < ApplicationRecord
  belongs_to :event
  validates :name, :phone, :email, presence: true 
end
