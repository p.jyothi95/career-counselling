class User < ApplicationRecord
  has_many :user_events
  has_many :assignments
  ROLES = ['staff', 'user']

  validates :name, presence: true
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable


end
