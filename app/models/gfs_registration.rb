class GfsRegistration < ApplicationRecord
serialize :financial_service_intrested, JSON

validates :name, :email, :terms_conditions,  presence: true

end
