class Question < ApplicationRecord
  #CATEGORIES = ['science', 'maths', 'reasoning', 'interests']
  enum q_type: { single_answer: 0, multiple_answer: 1, theory: 2 }
  before_save :downcase_category

  has_many :answers, dependent: :destroy
  has_many :section_questions


  validates :text, presence: true
 
  def downcase_category
    self.category = self.category.downcase if self.category.present?
  end
end
