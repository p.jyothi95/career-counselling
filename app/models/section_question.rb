class SectionQuestion < ApplicationRecord
  belongs_to :question
  belongs_to :section
end
