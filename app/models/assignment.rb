class Assignment < ApplicationRecord
  belongs_to :test
  belongs_to :user, optional: true
  #belongs_to :guest_user

  STATUSES = ['started', 'completed', 'reviewed']

  validates :due_date, presence: true

  scope :unarchived, -> {where(archived: false)}
  scope :pending, -> {where(status: nil)}
  scope :completed, -> {where(status: 'completed')}
  scope :reviewed, -> {where(status: 'reviewed')}

  def assignee
    if self.user_id.present?
      User.find self.user_id
    else
      GuestUser.find self.guest_user_id
    end
  end
end
