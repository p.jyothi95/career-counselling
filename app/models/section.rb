class Section < ApplicationRecord
  belongs_to :test
  has_many :section_questions

  scope :unarchived, -> { where(archived: false) }

  validates :name, presence: true
end
