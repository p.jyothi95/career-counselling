class StudentFeedbacksController < ApplicationController

  skip_before_action :authenticate_user!

  layout 'feedback'
  
  def new
    @feedback = StudentFeedback.new(event_id: params[:event_id])
  end

  def index
    @feedbacks = StudentFeedback.all
  end

  def create
    @feedback = StudentFeedback.new(feedback_params)
    if @feedback.save
      redirect_to event_feedback_path(@feedback.event_id, @feedback.id)
    else
      render action: 'new'
    end
  end

  def show
  end

  def destroy
    @feedback = StudentFeedback.find(params[:id])
    @feedback.destroy
    redirect_to feedbacks_path
  end
  
  private 
    def feedback_params
      params.require(:student_feedback).permit(:name, :date, :phone, :email, :program_title, :organisation, :suggestions, :top_3_things, :expirence, :implementation_on_day_to_day, :rsvp, :event_id)
    end

end
