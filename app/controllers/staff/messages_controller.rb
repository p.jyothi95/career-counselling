class Staff::MessagesController < ApplicationController

  def create
    @user = User.find(params[:user_id])
    @message = Message.new(message_params)
    @message.discussion_id = @user.id
    @message.sender_id = current_user.id
    @message.save

    respond_to do |format|
      format.html {
        redirect_to staff_get_messages_path(user_id: @user.id)
      }
      format.js {get_messages}
    end
  end
  
  def index
    get_users
    if params[:user_id].present?
      @user = User.find(params[:user_id])
      @messages = Message.where(:discussion_id => @user.id).order(:created_at)
    end
  end

  def get_messages
    @user = User.find(params[:user_id])
    @messages = Message.where(:discussion_id => @user.id).order(:created_at)
    @messages.where(sender_id: @user.id).update_all(is_read: true)
    get_users
  end

  def get_users
    @users = User.all
  end

  private
  def message_params
    params.require(:message).permit(:text)
  end

end
