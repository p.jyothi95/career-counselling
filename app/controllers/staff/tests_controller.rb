class Staff::TestsController < ApplicationController
  
  def new
    @test = Test.new
  end

  def create
    @test = Test.new(test_params)
    if @test.save
      redirect_to staff_test_path(@test)
    else
      render action: :new
    end
  end

  def index
     @tests = Test.where(archived: false).order(created_at: :desc)
  end

  def show
    @sections = Section.where(test_id: params[:id], archived: false)
    @test = Test.find(params[:id])
  end

  def destroy
    @test = Test.find(params[:id])
    @test.archived = true
    @test.save
    redirect_to staff_tests_path
  end

  def print
    @test = Test.find(params[:id])
    @sections = Section.where(test_id: params[:id], archived: false)
    render layout: 'print'
  end

  def upload_file
  end

  def upload
    @test = Test.find(params[:id])
    current_section = nil
    CSV.parse(params[:csv].read).each do |row|
      next if row.compact.count == 0

      if row.compact.count <= 2
        current_section = Section.create!(
          name: row[0],
          guidelines: row[1],
          test_id: @test.id,
          category: '_' + Time.now.to_i.to_s + '_' + rand(1000).to_s
        )
      end

      if row.compact.count > 2
        question = Question.create!(
          q_type: row[0],
          text: row[1],
          category: current_section.category
        )

        correct_answer_indexes = row[2].split(',').map &:strip
        [3, 4, 5, 6].each_with_index do |key, index|
          question.answers.create!(description: row[key], is_correct: correct_answer_indexes.include?((index+1).to_s)) if row[key].present?
        end

        SectionQuestion.create(section_id: current_section.id, question_id: question.id)
      end
    end
    redirect_to staff_test_path(@test.id)
  end

  private
    def test_params
      params.require(:test).permit(:name, :duration, :guidelines)
    end

end
