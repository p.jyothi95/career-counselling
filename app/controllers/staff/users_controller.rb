class Staff::UsersController < ApplicationController

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    @user.role = "staff"
    if @user.save 
      redirect_to staff_users_path
      else
      render action: :new
    end
  end

  def index
    @users = User.where(archived: false).order(created_at: :desc)
    @users = @users.paginate(:page => params[:page], :per_page => 20)
  end

  def update
    @user = User.find(params[:id])
    if @user.update_attributes(user_params)
        flash[:success]="Successfully updated"
        redirect_to staff_user_path()
      else
        render 'edit'
    end
  end 

  def edit
    @user = User.find(params[:id])
  end

  def show
    @user = User.find(params[:id])
    @assignments = Assignment.where(:user_id => params[:id], archived: false, status: nil)
  end

  def destroy
    @user = User.find(params[:id])
    @user.archived = true
    @user.email = "deleted-#{Time.now.to_i}-#{@user.email}"
    @user.save
    redirect_to staff_users_path
  end

  private
    def user_params
      params.require(:user).permit(:name, :phone_no, :address, :password, :password_confirmation, :email, :role).tap do |p|
        if p[:password].empty? and p[:password_confirmation].empty?
          p.delete :password
          p.delete :password_confirmation
        end
      end
    end

end
