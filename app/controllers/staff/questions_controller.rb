class Staff::QuestionsController < ApplicationController

  def new
     @question = Question.new
  end

  def create
    @question = Question.new(question_params)
    if @question.save
      answers_params.each do |i, ap|
        @answer = Answer.new(ap)
        if @answer.description.present?
          @answer.question_id = @question.id
          @answer.save
        end
      end

      redirect_to staff_questions_path
    else
      render action: :new
    end
  end
  
  def index
    @page = params[:page].to_i
    @per_page = 10
    @questions = Question.where(archived: false).order(created_at: :desc).limit(@per_page).offset(@page * @per_page)
    @next = true if (Question.where(archived: false).count/@per_page.to_f)-1 > @page
  end

   def destroy
    @question = Question.find(params[:id])
    @question.archived = true
    @question.save
    redirect_to staff_questions_path
  end

  def upload_file
  end

  def upload
    if params[:csv].present?
      CSV.parse(params[:csv].read, headers: true).each do |row|
        question = Question.create!(
          text: row['question'],
          q_type: row['question_type'],
          category: row['category'],
        )

        %w(ans1 ans2 ans3 ans4).each do |ans_key|
          question.answers.create!(description: row[ans_key], is_correct: row['correct_answer'] == ans_key) if row[ans_key].present?
        end
      end

      redirect_to staff_questions_path
    else
      flash[:notice] = "Please upload the file"
      render action: :upload_file
    end
  end

  private
    def question_params
      params.require(:question).permit(:q_type, :category, :text)
    end

    def answers_params
      params.permit(answers: [:description, :is_correct])[:answers]
    end
end
