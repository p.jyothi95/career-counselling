class Staff::AssignmentsController < ApplicationController
  
  def new
    @assignment = Assignment.new
  end

  def create
    @assignment = Assignment.new(assignment_params)
    @assignment.user_id = params[:user_id]
    if @assignment.save
      redirect_to staff_user_path(params[:user_id])
    else
      render action: :new
    end
  end

  def destroy
    @assignment = Assignment.find(params[:id])
    @assignment.archived = true
    @assignment.save
    redirect_to staff_user_path(params[:user_id])
  end
  
  def review
    @assignment = Assignment.find(params[:id])
    @test = Test.find(@assignment.test_id)
    @sections = Section.where(test_id: @test.id, archived: false)
    @result = Result.find_by(assignment_id: @assignment.id) || Result.new(answers: {})
  end

  def review_feedback
    @assignment = Assignment.find(params[:id])
    @assignment.status = "reviewed"
    @assignment.feedback = params[:assignment][:feedback]
    @assignment.save
    redirect_to staff_dashboard_path
  end

  private
  def assignment_params
    params.require(:assignment).permit(:due_date, :test_id)
  end

end
