class Staff::SectionsController < ApplicationController
  
  def new
    @section = Section.new
  end

  def create 
    @questions = Question.all
    @section = Section.new(section_params)
    @section.test_id = params[:test_id]
    if @section.save
      redirect_to staff_test_path(params[:test_id])
    else
      render action: :new
    end
  end

  def questions
    @section = Section.find params[:id]
    @questions = Question.where archived: false, category: @section.category

    @section_questions = SectionQuestion.where(section_id: @section.id)
    @selected_questions = Question.where(id: @section_questions.collect(&:question_id))
    @other_questions = Question.where(category: @section.category, archived: false).where.not(id: @section_questions.collect(&:question_id))
    @question = Question.last
  end

  def questions_update
    @section = Section.find params[:id]

    # Insert each question id if already not present
    params[:question_ids].each do |question_id|
      unless SectionQuestion.find_by_section_id_and_question_id(@section.id, question_id).present?
        SectionQuestion.create(section_id: @section.id, question_id: question_id)
      end
    end

    # remoe question ids other than selected
    SectionQuestion.where(section_id: @section.id).where.not(question_id: params[:question_ids]).destroy_all

    redirect_to staff_test_path(params[:test_id])
  end

  def destroy
    @section  = Section.find(params[:id])
    @section.archived = true
    @section.save
    redirect_to staff_test_path(params[:test_id])
  end

  private
    def section_params
      params.require(:section).permit(:name, :category, :guidelines)
    end
  
end
