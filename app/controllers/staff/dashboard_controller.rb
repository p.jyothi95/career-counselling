class Staff::DashboardController < ApplicationController

  def show
    @assignments = Assignment.where(status: 'completed')
    @completed_assignments = Assignment.where(status: 'reviewed')
  end

  def pending_reviews
    @assignments = Assignment.where(status: 'completed').order(created_at: :desc)
  end

  def completed_reviews
    @assignments = Assignment.where(status: 'reviewed').order(created_at: :desc)
  end
  
end
