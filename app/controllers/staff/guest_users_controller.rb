class Staff::GuestUsersController < ApplicationController
  
  def index
    @guest_users = GuestUser.all
  end

  def show
    @guest_user = GuestUser.find(params[:id])
  end

  def destroy
    @guest_user = GuestUser.find(params[:id])
    @guest_user.destroy()
    redirect_to staff_guest_users_path
  end
end
