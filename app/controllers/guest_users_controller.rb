class GuestUsersController < ApplicationController

  skip_before_action :authenticate_user!

  layout 'feedback'
  
  def new
    @guest_user = GuestUser.new
  end
 
  def create
    @guest_user = GuestUser.new(guest_user_params)
    @guest_user.save
  end

  private
    def guest_user_params
      params.require(:guest_user).permit(:name, :phone, :email, :address)
    end
    
end
