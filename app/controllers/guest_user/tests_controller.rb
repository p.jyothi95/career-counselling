class GuestUser::TestsController < ApplicationController
  before_action :authenticate_user!, only: [:show]

  def new_register
    if params[:slug].present?
      @test = Test.find_by(slug: params[:slug])
    else
      @test = Test.find(params[:id])
    end
    @guest_user = GuestUser.new
  end

  def register
    @test = Test.find(params[:id])
    @guest_user = GuestUser.new(guest_user_params)
    if @guest_user.save
      @assignment = Assignment.new(:test_id => @test.id, :guest_user_id => @guest_user.id)
      @assignment.due_date = Time.now
      @assignment.save!
      session[:assignment_id] = @assignment.id
      redirect_to guest_user_test_path(@test)
    else
      render action: :new_register
    end
  end

  def show
    if params[:slug].present?
      @test = Test.find_by(slug: params[:slug])
    else
      @test = Test.find(params[:id])
    end
    @sections = Section.where(test_id: @test.id, archived: false)
  end

  def start
    @assignment = Assignment.find(session[:assignment_id])
    if @assignment.due_date < Date.today
      flash[:notice] = "Test has been expired"
      redirect_to new_register_guest_user_test_path(@assignment.test_id)
      return
    end
    if @assignment.status.nil?
      @assignment.status = "started"
      @assignment.start_time = Time.now
      @assignment.save
    end
    redirect_to started_guest_user_test_path(@assignment.test_id) 
  end

  def started
    @assignment = Assignment.find(session[:assignment_id])
    @section_index = 0
    @section = @assignment.test.sections.unarchived[@section_index]
    elapased_time = ((Time.now - @assignment.start_time) / 60).to_i
    @time_remaining = @assignment.test.duration - elapased_time
    @count = @assignment.test.sections.unarchived.count
  end

  def next
    @assignment = Assignment.find(session[:assignment_id])
    save_answers
    @section_index = params[:section_index].to_i
    @section = @assignment.test.sections.unarchived[@section_index]
    @count = @assignment.test.sections.unarchived.count
  end

  def complete
    @assignment = Assignment.find(session[:assignment_id])
    @assignment.end_time = Time.now
    @assignment.status = "completed"
    @assignment.save
    redirect_to completed_guest_user_test_path(@assignment.test_id) 
  end

  def completed
  end
 

  private
    def guest_user_params
      params.require(:guest_user).permit(:name, :address, :phone, :email)
    end

    def save_answers
      result = Result.where(assignment_id: @assignment.id).first_or_initialize
      result.answers ||= {}
      result.answers.merge! params.permit(answers: {})[:answers] if params[:answers].present?
      result.save!
    end
end
