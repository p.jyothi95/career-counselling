class EventsController < ApplicationController
  before_action :authenticate_user!, only: [:register]

  def new
    @event = Event.new
  end

  def create
    @event = Event.new(event_params)
    upload_images
    if @event.save
      redirect_to event_path(@event)
    else
      render action: :new
    end
  end

  def index
    @events = Event.all.order(created_at: :desc)
  end

  def show
    @event = Event.find(params[:id])
    #@user_events = UserEvent.where(event_id: @event.id).joins(:user).where(users: {archived: false})
    #@user_events = @user_events.paginate(:page => params[:page], :per_page => 5)
    @event_registrations = EventRegistration.where(event_id: @event.id)
  end

  def print_qr
    @event = Event.find(params[:id])
    #@qr = RQRCode::QRCode.new( new_event_feedback_url(@event), :size => 10, :level => :h )
    #@qr = RQRCode::QRCode.new( "https://giga-business-plan.herokuapp.com/events/93/registrations/new", :size => 10, :level => :h )
    @qr = RQRCode::QRCode.new( "https://giga-business-plan.herokuapp.com/events/93/details", :size => 10, :level => :h )
    render layout: 'print'
  end

  def edit
    @event = Event.find(params[:id])
  end

  def update
    @event = Event.find(params[:id])
    upload_images
    if @event.update_attributes(event_params)
      flash[:success]="Successfully updated"
      redirect_to event_path(@event)
    else
      render 'edit'
    end
  end

  def destroy
    @event = Event.find(params[:id])
    @event.destroy
    redirect_to events_path
  end

  def register
    @event = Event.find(params[:id])
    @user_event = UserEvent.new
    @user_event.user_id = current_user.id
    @user_event.event_id = @event.id
    @user_event.save
    redirect_to details_event_path(@event)
    
  end

  def registered_users
    @event = Event.find(params[:id])
    #@user_events = UserEvent.where(event_id: @event.id).joins(:user).where(users: {archived: false})
    @event_registrations = EventRegistration.where(event_id: @event.id)
  end

  def print_reg_users
    @event = Event.find(params[:id])
    @event_registrations = EventRegistration.where(event_id: @event.id)
    render layout: 'print'

  end

  def details
    @event = Event.find(params[:id])
    #@user_events = UserEvent.where(event_id: @event.id).joins(:user).where(users: {archived: false})
    #@user_events = @user_events.paginate(:page => params[:page], :per_page => 5)
    @event_registrations = EventRegistration.where(event_id: @event.id)
    render layout: 'landing'
    
  end

  def listing
    @events = Event.where(category: [params[:category], 'general']).order(created_at: :desc)
    render layout: 'landing'
  end

  private
    def event_params
      params.require(:event).permit(:name, :description, :event_date, :reg_end_date, :category, :venue, :duration, :is_recurring, :frequency, :recurring_end_date, :map_embed_code, :speaker, :workshop_type)
    end
  
    def upload_images
      if params[:event][:image].present? and @event.valid?
        image = Cloudinary::Uploader.upload(params[:event][:image], options = {})
        @event.image = image["secure_url"]
      end
      if params[:event][:reg_image].present? and @event.valid?
        reg_image = Cloudinary::Uploader.upload(params[:event][:reg_image], options = {})
        @event.reg_image = reg_image["secure_url"]
      end
      if params[:event][:speaker_image].present? and @event.valid?
        speaker_image = Cloudinary::Uploader.upload(params[:event][:speaker_image], options = {})
        @event.speaker_image = speaker_image["secure_url"]
      end
    end
end
