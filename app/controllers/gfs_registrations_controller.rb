class GfsRegistrationsController < ApplicationController
  skip_before_action :authenticate_user!


  def new
    @gfs_reg = GfsRegistration.new
  end

  def create
    @gfs_reg = GfsRegistration.new(gfs_params)
    @gfs_reg.employment_detail
    upload_files
    if @gfs_reg.save
      flash[:success]="Successfully registered"
      redirect_to staff_dashboard_path
    else
      render action: :new
    end
  end

  def gfs_terms
  end

  private
    def gfs_params
      params.require(:gfs_registration).permit(:name, :date_of_birth, :state, :city, :email, :mobile, :phone, :martial_status, :dependents, :citizen, :pan, :expirence, :income, :amount, :terms_conditions, :outstanding_loan, :emi, :financial_service_intrested, :references, :revenue, :money_need, :itr_years, :profitable, :balance_tenure)
    end
  
    def upload_files
      if params[:gfs_registration][:sanction_letter_attachment].present? and @gfs_reg.valid?
        sanction_letter_attachment = Cloudinary::Uploader.upload(params[:gfs_registration][:sanction_letter_attachment], {
           folder: 'files/'
          })
        @gfs_reg.sanction_letter_attachment = sanction_letter_attachment["secure_url"]
      end
      if params[:gfs_registration][:funding_attachment].present? and @gfs_reg.valid?
        funding_attachment = Cloudinary::Uploader.upload(params[:gfs_registration][:funding_attachment], folder: 'files/')
        @gfs_reg.funding_attachment = funding_attachment["secure_url"]
      end
     
    end

end
