class EventRegistrationsController < ApplicationController
  skip_before_action :authenticate_user!

  def new
    @event = Event.find(params[:event_id])
    if @event.id == 54
      redirect_to "https://imjo.in/Ug7789"
      return
    end
    @event_registration = EventRegistration.new
  end

  def create
    @event = Event.find(params[:event_id])
    @event_registration = EventRegistration.new(event_registration_params)
    session[:event_id] = @event.id
    @event_registration.event_id = @event.id
    @event_registration.save
    redirect_to details_event_path(@event)
  end

  private
  def event_registration_params
    params.require(:registration).permit(:name, :mobile, :company, :email, extra: {})
  end
end
