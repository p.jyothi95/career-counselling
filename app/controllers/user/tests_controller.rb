class User::TestsController < ApplicationController
  before_action :authenticate_user!, only: [:register]

  def register
    @test = Test.find_by(slug: params[:slug])
    @assignment = Assignment.new(:test_id => @test.id, :user_id => current_user.id)
    @assignment.due_date = Time.now
    @assignment.save!
    session[:assignment_id] = @assignment.id
    redirect_to user_assignment_path(session[:assignment_id])
  end

end
