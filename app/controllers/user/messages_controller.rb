class User::MessagesController < ApplicationController
  
  def create
    @message = Message.new(message_params)
    @message.discussion_id = current_user.id
    @message.sender_id = current_user.id
    @message.save
    respond_to do |format|
      format.html {
        redirect_to user_messages_path
      }
      format.js {get_messages}
    end
    
  end

  def index
    @messages = Message.where(:discussion_id => current_user.id).order(:created_at)
  end

  def get_messages
    @messages = Message.where(:discussion_id => current_user.id).order(:created_at)
    @messages.where.not(sender_id: current_user.id).update_all(is_read: true)
  end

  private
  def message_params
    params.require(:message).permit(:text)
  end

end
