class User::AssignmentsController < ApplicationController
  before_action :authenticate_user!, only: [:show]
  
  def index
    @assignments = current_user.assignments.unarchived.pending.where("due_date >= ?", Date.today).order(created_at: :desc)
  end

  def show
    @assignment = Assignment.find(params[:id])
  end

  def start
    @assignment = Assignment.find(params[:id])
    if @assignment.due_date < Date.today
      flash[:notice] = "Test has been expired"
      redirect_to user_dashboard_path
      return
    end
    if @assignment.status.nil?
      @assignment.status = "started"
      @assignment.start_time = Time.now
      @assignment.save
    end
    redirect_to started_user_assignment_path(@assignment) 
  end

  def started
    @nosidebar = true
    @assignment = Assignment.find(params[:id])
    @section_index = 0
    @section = @assignment.test.sections.unarchived[@section_index]
    elapased_time = ((Time.now - @assignment.start_time) / 60).to_i
    @time_remaining = @assignment.test.duration - elapased_time
    @count = @assignment.test.sections.unarchived.count
  end

  def next
    @assignment = Assignment.find(params[:id])
    save_answers
    @section_index = params[:section_index].to_i
    @section = @assignment.test.sections.unarchived[@section_index]
    @count = @assignment.test.sections.unarchived.count
  end

  def complete
    @assignment = Assignment.find(params[:id])
    @assignment.end_time = Time.now
    @assignment.status = "completed"
    @assignment.save
    redirect_to completed_user_assignment_path(@assignment) 
  end

  def completed
  end

  def reviewed
    @assignments = Assignment.where(:user_id => current_user.id, archived: false, status: ['completed', 'reviewed']).order(created_at: :desc)
  end

  def show_reviewed_test
    @assignment = Assignment.find(params[:id])
  end


  private
    def save_answers
      result = Result.where(assignment_id: @assignment.id).first_or_initialize
      result.answers ||= {}
      result.answers.merge! params.permit(answers: {})[:answers] if params[:answers].present?
      result.save!
    end
end
