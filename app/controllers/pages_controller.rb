class PagesController < ApplicationController
  layout "landing"
  
  def home
    @upcoming = Event.active.order(event_date: :asc)
    @expires = Event.expired.order(event_date: :desc)
    @events = @upcoming + @expires
  end
  
end
