Rails.application.routes.draw do
  root to: 'pages#home'
  get 'ourwork' => 'pages#ourwork', as: :ourwork
  devise_for :users
  
  resources :gfs_registrations do
    collection do
      get :gfs_terms
    end
  end
  namespace :user do
    resources :messages do
      collection do
        get :get_messages
      end
    end
    resources :tests 
    resource :dashboard, controller: :dashboard
    resources :assignments do
      member do
        post :start
        get :started
        post :next
        get :complete
        get :completed
        get :show_reviewed_test
      end
      collection do
        get :reviewed
      end
    end
  end

  namespace :staff do
    resources :messages do
      collection do
        get :get_messages
        get :get_users
      end
    end
    resources :guest_users
    resource :dashboard, controller: :dashboard do 
      collection do
        get :pending_reviews
        get :completed_reviews
      end
    end
    resources :users do
      resources :assignments do
        member do
          get :review
          put :review_feedback
        end
      end
    end
    resources :questions do
      collection do
        get :upload_file
        post :upload
      end
    end
    resources :tests do
      member do
        get :upload_file
        post :upload
        get :print
      end
      resources :sections do
        member do
          get :questions
          put :questions_update
        end
      end
    end
  end

  namespace :guest_user do
    resources :tests, only: [:show] do
      member do
        get :new_register
        post :register
        post :start
        get :started
        post :next
        get :complete
        get :completed

      end
    end
  end
  
  resources :events do
    member do
      get :register
      get :registered_users
      get :details
      get :print_qr
      get :print_reg_users
    end
    collection do
      get ':category/listing' => 'events#listing', as: :listing
    end

    resources :feedbacks, :controller => "student_feedbacks"
    resources :registrations, :controller => "event_registrations"
  end


  get '/online/:slug' => 'user/tests#register', as: :online_test
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
