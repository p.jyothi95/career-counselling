namespace :events do
  desc "Creates new event for ended recurring events" 
  task recurring: :environment do
    events = Event.where(is_recurring: true,
                         event_date: Date.today.beginning_of_day..Date.today.end_of_day,
                         next_event_scheduled: false).where("recurring_end_date > ?", Date.today)

    puts "Processing #{events.count} events"
    events.each do |event|
      new_event = event.dup.tap do |e|
        e.event_date += e.frequency_duration
        e.reg_end_date += e.frequency_duration
      end
      new_event.save

      event.update next_event_scheduled: true
    end
    puts "Done!!"
  end
end